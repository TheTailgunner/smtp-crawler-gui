// -*- C++ -*-
#pragma once

#include <utility>
#include <vector>
#include <iterator>
#include <algorithm>
#include <cstdint>

#include "tcp-client.h"
#include "smtp-parser.h"
#include "smtp-response.h"

namespace crawler {
  namespace smtp {
    
    template <typename IOLayer = crawler::network::tcp_client>
    class smtp_stream {
    public:
      smtp_stream(IOLayer &_io): m_io{_io} { }

      smtp_stream &operator>>(std::vector<smtp_response> &_resps) {
	while (!m_parser.try_parse(m_io.read(),
				   _resps))
	  ;
	return *this;
      }

      smtp_stream &operator<<(const std::string &_command) {
	std::string cr_lf_command = add_cr_lf(_command);
	std::vector<std::uint8_t> data(cr_lf_command.cbegin(), cr_lf_command.cend());
	m_io.write(std::move(data));
	return *this;
      }
    private:
      IOLayer &m_io;
      smtp_parser m_parser;

      std::string add_cr_lf(const std::string &_str) { return _str + "\r\n"; }
    };
  }
}
