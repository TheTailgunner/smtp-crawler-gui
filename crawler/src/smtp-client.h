// -*- C++ -*-

#pragma once

#include <cstdlib>
#include <string>
#include <vector>
#include "tcp-client.h"
#include "smtp-stream.h"
#include "smtp-response.h"

namespace boost {
  namespace asio {
class io_service;
  }
}

namespace crawler {
namespace smtp {
  
class smtp_client {
public:
  smtp_client(boost::asio::io_service &);
  ~smtp_client();

  bool try_connect(const std::string &,
		   std::size_t = DEFAULT_SMTP_PORT) noexcept;
  bool try_login(const std::string &,
	     const std::string &);
  void terminate() noexcept;

  void set_connect_timeout(std::size_t _sec) { m_network.set_timeout(_sec); }
private:
  using smtp_response_list = std::vector<smtp_response>;
  using tcp_smtp_stream = smtp_stream<crawler::network::tcp_client>;

  static const std::size_t DEFAULT_SMTP_PORT {25};

  bool provides_tls(const smtp_response_list &);
  void start_tls();

  bool provides_auth_plain(const smtp_response_list &);
  bool auth_plain(const std::string &,
		  const std::string &);

  std::string encode_base64(const std::string &);
  
  smtp_response_list say_ehlo();

  bool m_connected;
  crawler::network::tcp_client m_network;
};
  
}
}
