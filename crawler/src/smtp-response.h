//-*- C++ -*-

#pragma once

#include <string>

namespace crawler {
  namespace smtp {
    class smtp_response {
    public:
      enum class response_status { positive_completed, positive_intermediate,
	  negative_transient, negative_permanent
	  };
      
      smtp_response(const std::string &,
		    const std::string &);
      bool good() const {
	return response_status::positive_completed == m_code ||
	  response_status::positive_intermediate == m_code;
      }
      response_status status() const { return m_code; }
      const std::string &text() const { return m_text; }
    private:
      response_status parse_status(const std::string &) const;
      
      response_status m_code;
      std::string m_text;
    };
  }
}
