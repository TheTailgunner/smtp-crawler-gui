#include "smtp-response.h"
#include <stdexcept>

namespace crawler {
  namespace smtp {

    using response_status = smtp_response::response_status;
    
    smtp_response::smtp_response(const std::string &_code,
				 const std::string &_text): m_code(parse_status(_code)),
							    m_text(_text) {
    }

    response_status smtp_response::parse_status(const std::string &_code) const {
      if (_code.length() != 3)
	throw std::runtime_error("smtp_response: wrong response code length");
      
      char first = _code.at(0);

      switch (first) {
      case '2': return response_status::positive_completed;
      case '3': return response_status::positive_intermediate;
      case '4': return response_status::negative_transient;
      case '5': return response_status::negative_permanent;
      default: throw std::runtime_error("smtp_response: wrong code status");
      }
    }
  }
}
