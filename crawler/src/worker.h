// -*- C++ -*-

#pragma once

#include "shared-queue.h"
#include "smtp-client.h"
#include <cstdlib>
#include <boost/thread/thread.hpp>
#include <boost/atomic.hpp>
#include <vector>
#include <utility>

namespace boost {
  namespace asio {
    class io_service;
  }
}

namespace crawler {
  using namespace boost::asio;
  
  class worker {
  public:    
    struct task {
      std::string ip;
      std::uint16_t port;
    };

    using login_pair = std::pair<std::string, std::string>;
    using result_pair = std::pair<worker::task, worker::login_pair>;

    enum class mode { to_end, stop_on_first };
    
    worker(io_service &,
	   std::size_t = worker::DEFAULT_THREAD_COUNT);
    ~worker();

    void run();
    void block_until_complete();
    void stop() noexcept;

    void add_login_pair(const std::string &,
			const std::string &);
    void push_task(task);

    worker::mode current_mode() const { return m_mode; }
    void set_mode(worker::mode _m) { m_mode = _m; }

    void set_thread_count(std::size_t _count) { m_thread_count = _count; }

    std::vector<result_pair> results() const { return m_results; }

    void set_connect_timeout(std::size_t _sec) { m_smtp_timeout = _sec; }
  private:
    static const std::size_t DEFAULT_THREAD_COUNT{1};
    using task_queue = crawler::utils::shared_queue<task>;

    void process_server(crawler::smtp::smtp_client &,
			worker::task);
    void push_result(worker::task,
		     worker::login_pair);
    void do_work();

    boost::asio::io_service &m_io_service;
    task_queue m_tasks;
    boost::thread_group m_threads;
    std::size_t m_thread_count{DEFAULT_THREAD_COUNT};
    std::vector<login_pair> m_logins;
    std::vector<result_pair> m_results;

    boost::atomic<std::size_t> m_tasks_pending;
    boost::mutex m_mutex;
    boost::condition_variable m_stop_cond;
    bool m_need_stop;
    worker::mode m_mode;
    std::size_t m_smtp_timeout{crawler::network::tcp_client::DEFAULT_TIMEOUT_SEC};
  };
}
