#include <iostream>

#include <boost/asio.hpp>
#include <stdexcept>
#include <boost/program_options.hpp>

#include <string>
#include <cstdlib>
#include <stdexcept>
#include <fstream>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <map>
#include <utility>

#include "worker.h"
#include "easylogging++.h"

#include <thread>
#include "main-window.h"

namespace po =  boost::program_options;

INITIALIZE_EASYLOGGINGPP

po::variables_map parse_command_line(int argc, char **argv) {
  po::options_description descr("smtp-crawler opts");
  descr.add_options()
    ("help", "help message")
    ("color", "enable color log")
    ("workers", po::value<std::size_t>(), "set working threads count")
    ("credentials", po::value<std::string>(), "file containing login info")
    ("hosts", po::value<std::string>(), "file containig host")
    ("stop-on-first", "enable terminating on first succesfully found login pair")
    ("log-level", po::value<std::string>(), "log level: error, debug, info" "[info]")
    ("output", po::value<std::string>(), "result file name")
    ("connection-timeout", po::value<std::size_t>(), "connection timeout, sec [default: 1]")
    ;

  po::variables_map opts;
  po::store(po::parse_command_line(argc, argv, descr), opts);
  po::notify(opts);

  if (opts.count("help")) {
    std::cout << descr << std::endl;
    std::exit(0);
  }

  return opts;
}

void configure_logger(const po::variables_map &_opts) {
  el::Configurations default_conf;
  default_conf.setToDefault();
  default_conf.set(el::Level::Info, 
            el::ConfigurationType::Format, "%datetime %level %msg");
  default_conf.set(el::Level::Error, 
            el::ConfigurationType::Format, "%datetime %loc %msg");
  default_conf.set(el::Level::Debug, 
            el::ConfigurationType::Format, "%datetime %func:%loc %msg");

  el::Loggers::addFlag(el::LoggingFlag::HierarchicalLogging);
  el::Loggers::setLoggingLevel(el::Level::Info);

  if (_opts.count("color"))
    el::Loggers::addFlag(el::LoggingFlag::ColoredTerminalOutput);

  if (_opts.count("log-level")) {
    el::Level log_level = el::LevelHelper::convertFromString(
						   _opts["log-level"].as<std::string>().c_str()
						   );
    if (log_level != el::Level::Unknown)
      el::Loggers::setLoggingLevel(log_level);
    else {
      std::cerr << "invalid log-level - fallback to default" << std::endl;
    }
  }

  el::Loggers::reconfigureLogger("default", default_conf);
  
  LOG(INFO) << "Logger configured";
}

int main(int argc, char **argv) {
  boost::asio::io_service service;
  boost::asio::io_service::work work(service);
  std::thread thread([&service] { service.run(); });

  crawler::gui::main_window win(service);
  
  try {
    auto opts = parse_command_line(argc, argv);
    configure_logger(opts);
  } catch (const std::exception &_exc) {
    std::cerr << _exc.what() << std::endl;
  }

  Fl::run();
  
  service.stop();
  thread.join();

  return 0;
}
