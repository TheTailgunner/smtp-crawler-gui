#include "smtp-parser.h"

namespace crawler {
  namespace smtp {

    void smtp_parser::reset() {
      m_current_code.clear();
      m_current_text.clear();
      m_last_sym = 0;
      m_state = parser_state::response_code;
      m_awaits_multiline = false;
      m_multiline_last = false;
    }
    
    bool smtp_parser::try_parse(const std::vector<std::uint8_t> &_data,
				std::vector<smtp_response> &_out_resps) {
      
      for (auto sym : _data) {

	switch (m_state) {
	case parser_state::response_code:
	  m_current_code += sym;
	  if (m_current_code.length() == 3) {
	    m_state = parser_state::delimiter;
	  }
	  break;

	case parser_state::delimiter:
	  if ('-' == sym)
	    m_awaits_multiline = true;
	  else if (m_awaits_multiline && ' ' == sym)
	    m_multiline_last = true;
	  
	  m_state = parser_state::response_message;
	  break;

	case parser_state::response_message:
	  if (sym != '\r')
	    m_current_text += sym;
	  else
	    m_state = parser_state::terminator;
	  break;
	case parser_state::terminator:
	  if ('\n' == sym &&
	      '\r' == m_last_sym) {
	    _out_resps.push_back(smtp_response(m_current_code,
						m_current_text));

	    if (!m_awaits_multiline ||
		m_multiline_last) {
	      reset();
	      return true;
	    } else if (m_awaits_multiline) {
	      m_current_text.clear();
	      m_current_code.clear();
	    }
	  }
	  m_state = parser_state::response_code;
	  break;
	}
	m_last_sym = sym;
      }
      return false;
    }
    
    }
}
