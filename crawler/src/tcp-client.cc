#include "tcp-client.h"
#include <string>
#include <future>
#include "easylogging++.h"
#include <boost/asio/use_future.hpp>
#include <chrono>

namespace crawler {
  namespace network {
    using namespace boost::asio;
    
    tcp_client::tcp_client(boost::asio::io_service &_serv):
      m_ssl_enabled(false),
      m_ssl_context(ssl::context::sslv23),
      m_socket(_serv, m_ssl_context),
      m_resolver(_serv) {
      m_ssl_context.set_default_verify_paths();
    }

    void tcp_client::start_tls() {
      LOG(DEBUG) << "\t\tstarting tls";
      m_socket.set_verify_mode(ssl::verify_peer);
      LOG(DEBUG) << "\t\ttrying to handshake";
      m_socket.handshake(ssl::stream<ip::tcp::socket>::client);
      LOG(DEBUG) << "\t\thandshake done";
      m_ssl_enabled = true;
    }
    
    void tcp_client::connect(const std::string &_host,
			     std::uint16_t _port) {
      disconnect();

      LOG(DEBUG) << "\t\tconnect to " << _host << ":" << _port;

      ip::tcp::resolver::query res_query(_host, std::to_string(_port));
      ip::tcp::resolver::iterator endp_itr = m_resolver.resolve(res_query);

      auto fut = m_socket.lowest_layer().async_connect(*endp_itr,
					    boost::asio::use_future);
      
      if (fut.wait_for(std::chrono::seconds(m_timeout)) != std::future_status::timeout)
	fut.get();
      else
	throw timeouted_exception();
      
      LOG(DEBUG) << "\t\tconnected to " << _host << ":" << _port;
    }

    void tcp_client::write(const std::vector<std::uint8_t> &data) {
      if (!m_ssl_enabled)
	boost::asio::write(m_socket.next_layer(), boost::asio::buffer(data));
      else
	boost::asio::write(m_socket, boost::asio::buffer(data));
    }

    std::vector<std::uint8_t> tcp_client::read() {
      std::size_t avail = m_socket.lowest_layer().available();
      std::vector<std::uint8_t> data(avail);

      if (avail) {
	if (!m_ssl_enabled)
	  m_socket.next_layer().read_some(boost::asio::buffer(data));
	else {
	  m_socket.read_some(boost::asio::buffer(data));
	}
      }
      return data;
    }
    
    void tcp_client::disconnect() {
      if (m_ssl_enabled) {
	try {
	  m_socket.shutdown();
	} catch (boost::system::system_error &_exc) {
	  if (_exc.code() != boost::asio::error::eof)
	    throw;
	}
      }
      m_ssl_enabled = false;
      m_socket.lowest_layer().close();
      m_resolver.cancel();
    }
    
    tcp_client::~tcp_client() {
      try {
	disconnect();
      } catch (const std::runtime_error &_exc) {
	LOG(ERROR) << _exc.what();
      }
    }
  }
}
