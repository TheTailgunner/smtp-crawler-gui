// -*- C++ -*-

#pragma once

#include <queue>
#include <mutex>
#include <condition_variable>
#include <boost/noncopyable.hpp>

namespace crawler {
  namespace utils {
    
    template <typename T>
    class shared_queue: private boost::noncopyable {
    public:
      // shared_queue(shared_queue &&) = default;
      // shared_queue &operator=(shared_queue &&) = default;

      bool empty() const {
	std::lock_guard<std::mutex> guard(m_mutex);
	return m_queue.empty();
      }

      void push(const T &_el) {
	m_mutex.lock();
	
	m_queue.push(_el);
	
	m_mutex.unlock();
	m_cond.notify_one();
	m_notified = true;
      }

      void pop(T &_popped) {
	std::unique_lock<std::mutex> guard(m_mutex);

	while (m_queue.empty() &&
	       !m_notified)	// to prevent spurious wake-ups
	  m_cond.wait(guard);
	
	_popped = m_queue.front();
	m_queue.pop();
	m_notified = false;
      }
      
    private:
      mutable std::mutex m_mutex;
      std::condition_variable m_cond;
      std::queue<T> m_queue;
      bool m_notified{false};
    };
    
  }
}
