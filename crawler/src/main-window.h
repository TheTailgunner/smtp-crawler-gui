// -*- C++ -*-
#pragma once

#include "easylogging++.h"
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Simple_Counter.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_File_Chooser.H>
#include <FL/fl_ask.H>
#include <stdexcept>

#include "worker.h"

namespace boost {
  namespace asio {
    class io_service;
  }
}

namespace crawler {
  namespace gui {
    class main_window {
    public:
      main_window(boost::asio::io_service &);
      
      static void select_hosts_pressed(Fl_Widget *, void *);
      void select_hosts_pressed_cb(Fl_Widget *);

      static void select_logins_pressed(Fl_Widget *, void *);
      void select_logins_pressed_cb(Fl_Widget *);

      static void start_pressed(Fl_Widget *, void *);
      void start_pressed_cb(Fl_Widget *);

      static void stop_pressed(Fl_Widget *, void *);
      void stop_pressed_cb(Fl_Widget *);

      ~main_window();
    private:
      crawler::worker m_worker;
      
      Fl_Window *_p_main_win;
      
      Fl_Input *_p_hosts_input;
      Fl_Input *_p_creds_input;
      
      Fl_Button *_p_find_hosts;
      Fl_Button *_p_find_creds_button;
      
      Fl_Simple_Counter *_p_thread_counter;
      Fl_Simple_Counter *_p_timeout_counter;

      Fl_Button *_p_start_button;
      Fl_Button *_p_stop_button;

      bool _running{false};
      void read_logins(const char *);
      void read_hosts(const char *);
      void write_results();
    };
  }
}
