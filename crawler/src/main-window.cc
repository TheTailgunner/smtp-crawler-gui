#include "main-window.h"
#include <fstream>
#include <map>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <utility>

using namespace crawler::gui;

main_window::main_window(boost::asio::io_service &_serv): m_worker(_serv) {
  _p_main_win = new Fl_Window(380, 160, "SMTP Crawler");

  _p_hosts_input = new Fl_Input(60, 20, 260, 23, "IP:");
  _p_creds_input = new Fl_Input(60, 50, 260, 23, "Logins:");

  _p_find_hosts = new Fl_Button(325, 20, 40, 23, "...");
  _p_find_hosts->callback(select_hosts_pressed, (void *) this);
  
  _p_find_creds_button = new Fl_Button(325, 50, 40, 23, "...");
  _p_find_creds_button->callback(select_logins_pressed, (void *) this);

  _p_thread_counter = new Fl_Simple_Counter(5, 80, 80, 23, "Workers");
  _p_thread_counter->step(1.0);
  _p_thread_counter->value(1.0);
  _p_thread_counter->bounds(1, 9999);

  _p_timeout_counter = new Fl_Simple_Counter(130, 80, 80, 23, "Connnection timeout, sec");
  _p_timeout_counter->step(1.0);
  _p_timeout_counter->bounds(1, 9999);
  _p_timeout_counter->value(1.0);

  _p_start_button = new Fl_Button(5, 130, 185, 30, "Start");
  _p_start_button->callback(start_pressed, (void *) this);
  
  _p_stop_button = new Fl_Button(190, 130, 185, 30, "Stop");
  _p_stop_button->callback(stop_pressed, (void *) this);

  _p_main_win->end();
  
  _p_main_win->show();
}

void main_window::select_hosts_pressed(Fl_Widget *_pw, void *_pd) {
  static_cast<main_window*>(_pd)->select_hosts_pressed_cb(_pw);
}

void main_window::select_hosts_pressed_cb(Fl_Widget *_pw) {
  (void) _pw;
  char * filename = fl_file_chooser(".",
				    "CSV Files (*.csv)\tTXT files (*.txt)\t",
				    "Select hosts file", 0);
  if (filename)
    this->_p_hosts_input->value(filename);
}

void main_window::select_logins_pressed(Fl_Widget *_pw, void *_pd) {
  static_cast<main_window*>(_pd)->select_logins_pressed_cb(_pw);
}

void main_window::select_logins_pressed_cb(Fl_Widget *_pw) {
  (void) _pw;

  char * filename = fl_file_chooser(".",
				    "CSV Files (*.csv)\tTXT files (*.txt)\t",
				    "Select hosts file", 0);
  if (filename)
    this->_p_creds_input->value(filename);
}

void main_window::start_pressed(Fl_Widget *_pc, void *_pd) {
  static_cast<main_window*>(_pd)->start_pressed_cb(_pc);
}

void main_window::start_pressed_cb(Fl_Widget *_pc) {
  (void) _pc;

  if (_running)
    return;
  
  if (this->_p_hosts_input->size() == 0 ||
      this->_p_creds_input->size() == 0) {
    fl_alert("Error: missing input files");
    return;
  }

  try {
    read_logins(_p_creds_input->value());
    read_hosts(_p_hosts_input->value());
  } catch (const std::exception &_exc) {
    fl_alert("Cannot read input files: %s", _exc.what());
    return;
  }

  m_worker.set_thread_count(this->_p_thread_counter->value());
  m_worker.set_connect_timeout(this->_p_timeout_counter->value());
  
  m_worker.run();

  _running = true;
}

void main_window::stop_pressed(Fl_Widget *_pc, void *_pd) {
  static_cast<main_window*>(_pd)->stop_pressed_cb(_pc);
}

void main_window::stop_pressed_cb(Fl_Widget *_pc) {
  (void) _pc;
  if (!_running)
    return;
  
  m_worker.stop();

  try {
    write_results();
  } catch (const std::exception &_exc) {
    fl_alert("Error writing results: %s", _exc.what());
  }

  _running = false;
}

void main_window::write_results() {
  static const std::string OUTPUT_FILENAME = "result.txt";
  
  std::ofstream out;
  out.open(OUTPUT_FILENAME);

  if (!out.good()) {
    std::string err_msg = OUTPUT_FILENAME + ":" +
      std::string(strerror(errno));
    throw std::runtime_error(strerror(errno));
  }
  
  std::vector<crawler::worker::result_pair> results = m_worker.results();

  for (auto &_res : results) {
    out << _res.first.ip << ","
	<< _res.first.port << ","
	<< _res.second.first << ","
	<< _res.second.second << std::endl;
    out.flush();
  }

  out.close();
}

void main_window::read_logins(const char *_fn) {
  LOG(DEBUG) << "reading credentials";

  std::ifstream in;
  LOG(DEBUG) << "trying to open " << _fn;
  in.open(_fn);

  if (!in.good()) {
    std::string err_msg = std::string(_fn) + ":" +
      std::string(strerror(errno));
    throw std::runtime_error(err_msg);
  }

  std::string line;
  std::size_t line_no = 0;

  using filter_map = std::map<std::pair<std::string, std::string>, int>;
  filter_map filter;

  while (std::getline(in, line)) {
    ++line_no;
    std::vector<std::string> tokens;
    boost::algorithm::split(tokens, line,
			    boost::is_any_of(","));

    if (tokens.size() != 2) {
      LOG(WARNING) << _fn << ":" << line_no
	<< " wrong fields count";
      continue;
    }

    // separating unique login-password pairs
    filter.insert({ std::make_pair(tokens[0], tokens[1]), 0});
  }

  for (filter_map::const_iterator itr = filter.cbegin();
       itr != filter.cend();
       ++itr) {
    m_worker.add_login_pair(itr->first.first, itr->first.second);
  }
  
  in.close();
}

void main_window::read_hosts(const char *_fn) {
  std::ifstream in;
  LOG(DEBUG) << "trying to open " << _fn;
  in.open(_fn);
  
  if (!in.good()) {
    std::string err_msg = std::string(_fn) + ":" +
      std::string(strerror(errno));
    throw std::runtime_error(strerror(errno));
  }

  std::string line;
  std::size_t line_no{0};

  while (std::getline(in, line)) {
    ++line_no;
    std::vector<std::string> tokens;
    boost::algorithm::split(tokens, line,
			    boost::is_any_of(","));

    if (tokens.size() != 2) {
      LOG(WARNING) << _fn << ":" << line_no
	 << " wrong fields count";
      continue;
    }

    crawler::worker::task t;

    try {
      t.port = boost::lexical_cast<std::size_t>(tokens[1]);
    } catch (const boost::bad_lexical_cast &_exc) {
      LOG(WARNING) << _fn << ":" << line_no
		   << " wrong port value "
		   << _exc.what();
      continue;
    }

    t.ip = tokens[0];
    m_worker.push_task(t);
  }
  
  in.close();
}

main_window::~main_window() {
  m_worker.stop();
}
