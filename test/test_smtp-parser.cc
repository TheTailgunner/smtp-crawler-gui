#include <smtp-parser.h>
#include <gmock/gmock.h>
#include <iterator>
#include <algorithm>
#include <cstdint>
#include <utility>
#include <vector>
#include <smtp-response.h>

using namespace crawler::smtp;
using namespace ::testing;

std::vector<std::uint8_t> string_to_buffer(const std::string &_str) {
  std::vector<std::uint8_t> res;
  std::copy(_str.begin(), _str.end(), std::back_inserter(res));
  return std::move(res);
}

TEST(SMTP_Parser, StoresNonFinishedResponseInternal) {
  smtp_parser parser;
  std::vector<smtp_response> res;

  parser.try_parse(string_to_buffer("25"), res);
  ASSERT_THAT(parser.current_code_string(),
	      ::testing::Eq("25"));
  ASSERT_THAT(parser.state(), Eq(smtp_parser::parser_state::response_code));

  parser.try_parse(string_to_buffer("3 TE"), res);
  ASSERT_THAT(parser.current_text_string(), Eq("TE"));
}

TEST(SMTP_Parser, Parses_One_Line_Response) {
  smtp_parser parser;
  std::vector<smtp_response> res;
  
  bool parsed =  parser.try_parse(
				  string_to_buffer("250 TEST\r\n"),
				  res
						     );

  ASSERT_TRUE(res.size() == 1 && res[0].text() == "TEST");
  ASSERT_TRUE(parsed);
}

TEST(SMTP_Parser, Detects_MultiLine_Response) {
  smtp_parser parser;
  std::vector<smtp_response> res;
  ASSERT_FALSE(parser.awaits_multiline());

  parser.try_parse(string_to_buffer("250-8BITMIME\r"), res);
  ASSERT_TRUE(parser.awaits_multiline());
  parser.try_parse(string_to_buffer("\n250 STARTTLS\r\n"), res);

  ASSERT_THAT(res.size(), Eq(2));
  ASSERT_FALSE(parser.awaits_multiline());
}

TEST(SMTP_Parser, Parses_MultiLine_Response) {
  smtp_parser parser;
  std::vector<smtp_response> res;

  bool parsed = parser.try_parse(
				 string_to_buffer("250-8BITMIME\r\n"
						  "250 STARTTLS\r\n"),
				 res
				 );

  ASSERT_TRUE(parsed);
  ASSERT_THAT(res.size(), Eq(2));
}

TEST(SMTP_Parser, ReturnsFalse_WhenOne_Line_Response_IsNotComplete) {
  smtp_parser parser;
  std::vector<smtp_response> res;

  ASSERT_FALSE(parser.try_parse(
				string_to_buffer("250 8BITM"),
				res
				));
}

TEST(SMTP_Parser, ReturnsFalse_WhenMultilineResponse_IsNotComplete) {
  smtp_parser parser;
  std::vector<smtp_response> res;

  ASSERT_FALSE(parser.try_parse(
				string_to_buffer("250-8BITMIME\r\n"),
				res
				));
}
